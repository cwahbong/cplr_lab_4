import java.util.Scanner;

public class Io {

  private static Scanner scanner = new Scanner(System.in);

  public static void print(int i) {
    System.out.print(i);
  }

  public static void print(String s) {
    System.out.print(s);
  }

  public static void newline() {
    System.out.print("\n");
  }

  public static int scan() {
    return scanner.nextInt();
  }

}

