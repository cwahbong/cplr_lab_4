import argparse
import sys

import ply

import compiler.cmm as cmm
from compiler.cmm import jasm

""" The main module of lab_4.
    For its usage please see the argparser object.
"""

def write_asmcode(file, filename, code):
  classname = filename[filename.rfind('/')+1:filename.rfind('.')]
  file.write('.class public {}\n'.format(classname))
  for line in code:
    file.write('{}\n'.format(line))

argparser = argparse.ArgumentParser(description=""" Compile the cmm
  program into bass asm code.  It reads from stdin and writes to a file
  specified by the user by default. """
)
argparser.add_argument('-i', '--input', metavar="file_name", help="""
  specify input cmm program file by name instead of stdin. """
)
argparser.add_argument('-o', '--output', metavar="file_name", required=True, help="""
  specify output file by name if you instead of stdout. """
)
 
def __main__():
  args = argparser.parse_args()
  with open(args.input, 'r') if args.input else sys.stdin as input_file:
    content = ''.join(input_file)
  try:
    result = cmm.parser.parse(content)
    with open(args.output, 'w') as output_file:
      if result:
        write_asmcode(output_file, args.output, jasm.gen_code(result))
  except (ply.lex.LexError, SyntaxError) as e:
    sys.stderr.write(str(e)+"\n")

if __name__=='__main__':
  __main__()

