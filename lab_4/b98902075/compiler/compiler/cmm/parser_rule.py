import sys

""" Parser rule of `cmm' language.  Used to construct the cmm parser.
    Powered by PLY.  For its naming convention and usage please go to
    its official website, mentioned in compiler.cmm.__init__.
"""

def body(p):
  """ Get the body of the production rule.
  """
  return tuple(p[1:])

def setnode(type, p):
  """ Set the value of a nonterminal, and let it be a tree node.
  """
  p[0] = (type, body(p))

precedence = (
  ('left', 'LOGIC_OR'),
  ('left', 'LOGIC_AND'),
  ('left', 'EQ', 'NEQ'), 
  ('left', 'LEQ', 'LT', 'GEQ', 'GT'),
  ('left', 'PLUS', 'MINUS'),
  ('left', 'TIMES', 'DIVIDE'),
  ('right', 'UPLUS', 'UMINUS', 'LOGIC_NOT'),
)

def p_program(p):
  """ program : block_stmt
  """
  setnode("program", p)

def p_block_stmt(p):
  """ block_stmt : '{' a_var_decl a_stmt '}'
  """
  setnode("block_stmt", p)

def p_a_var_decl(p):
  """ a_var_decl : var_decl a_var_decl
                 | empty
  """
  setnode("a_var_decl", p)

def p_a_stmt(p):
  """ a_stmt     : stmt a_stmt
                 | empty
  """
  setnode("a_stmt", p)

def p_var_decl(p):
  """ var_decl      : INT a_bracket_num ID a_comma_id ';'
  """
  setnode("var_decl", p)

def p_a_bracket_num(p):
  """ a_bracket_num : '[' NUM ']' a_bracket_num
                    | empty
  """
  setnode("a_bracket_num", p)

def p_a_comma_id(p):
  """ a_comma_id    : ',' ID a_comma_id
                    | empty
  """
  setnode("a_comma_id", p)

def p_var(p):
  """ var                  : ID a_bracket_arith_expr
  """
  setnode("var", p)

def p_a_bracket_arith_expr(p):
  """ a_bracket_arith_expr : '[' arith_expr ']' a_bracket_arith_expr
                           | empty
  """
  setnode("a_bracket_arith_expr", p)

def p_stmt(p):
  """ stmt                   : var '=' arith_expr ';'
                             | IF '(' logic_expr ')' block_stmt 
                             | IF '(' logic_expr ')' block_stmt ELSE block_stmt
                             | WHILE '(' logic_expr ')' block_stmt
                             | BREAK ';'
                             | CONTINUE ';'
                             | SCAN '(' var a_comma_var ')' ';'
                             | PRINT '(' printable_expr a_comma_printable_expr ')' ';'
                             | PRINTLN '(' printable_expr a_comma_printable_expr ')' ';'
                             | block_stmt
  """
  setnode("stmt", p)

def p_a_comma_var(p):
  """ a_comma_var            : ',' var a_comma_var
                             | empty
  """
  setnode("a_comma_var", p)

def p_a_comma_printable_expr(p):
  """ a_comma_printable_expr : ',' printable_expr a_comma_printable_expr
                             | empty
  """
  setnode("a_comma_printable_expr", p)

def p_printable_expr(p):
  """ printable_expr : STR
                     | arith_expr
  """
  setnode("printable_expr", p)

def p_arith_expr(p):
  """ arith_expr : arith_expr PLUS arith_expr
                 | arith_expr MINUS arith_expr
                 | arith_expr TIMES arith_expr
                 | arith_expr DIVIDE arith_expr
                 | PLUS arith_expr                %prec UPLUS
                 | MINUS arith_expr               %prec UMINUS
                 | var
                 | NUM
                 | '(' arith_expr ')'
  """
  setnode("arith_expr", p)

def p_logic_expr(p):
  """ logic_expr : logic_expr LOGIC_OR logic_expr 
                 | logic_expr LOGIC_AND logic_expr
                 | LOGIC_NOT logic_expr
                 | arith_expr LEQ arith_expr
                 | arith_expr LT arith_expr
                 | arith_expr GEQ arith_expr
                 | arith_expr GT arith_expr
                 | arith_expr EQ arith_expr
                 | arith_expr NEQ arith_expr
                 | '[' logic_expr ']'
  """
  setnode("logic_expr", p)

def p_empty(p):
  """ empty :
  """
  setnode("empty", p)

def p_error(p):
  sys.stderr.write("Syntax error at token {}\n".format(str(p)))

