import inspect
import sys

class var_table:

  def __init__(self):
    self.__next_var_num = 1
    self.__scopes = []

  def scope_push(self):
    self.__scopes.append({})

  def scope_pop(self):
    scope = self.__scopes.pop()
    self.__next_var_num -= len(scope)

  def add_variable_tag(self, id, dim):
    scope = self.__scopes[-1]
    if id in scope:
      raise ValueError
    scope[id] = (self.__next_var_num, dim)
    self.__next_var_num += 1

  def get_variable_tag(self, id):
    for scope in reversed(self.__scopes):
      if id in scope:
        return scope[id]

class tag_generator:
  def __init__(self):
    self.__next_var_num = 0
  
  def get_next_tag(self):
    self.__next_var_num += 1
    return "T" + str(self.__next_var_num)

variables = var_table()
tags = tag_generator()
loop_tags = []

def gen_code(n):
  prefix = "c_"
  func = getattr(sys.modules[__name__], prefix+n[0])
  return func(n)

def c_program(n):
  """ The main program template.
  """
  return [
    ".method void <init>()",
    "\taload #0",
    "\tinvokespecial void <init>() @ Object",
    "\treturn",
    "",
    ".method public static void main(String[])"
  ] + gen_code(n[1][0]) + [
    "\treturn"
  ]

def c_block_stmt(n):
  variables.scope_push()
  try:
    return sum(map(gen_code, n[1][1:3]), [])
  finally:
    variables.scope_pop()

def c_a_var_decl(n):
  """ Combine multiple declarations.
  """
  return sum(map(gen_code, n[1]), [])

def c_a_stmt(n):
  """ Combine multiple statements.
  """
  return sum(map(gen_code, n[1]), [])

def c_var_decl(n):
  """ put variable into variable table and raise error if
      multiple declaration.
      This operation generates empty code.
  """
  def _a_bracket_num(n):
    if len(n[1])==1:
      return []
    elif len(n[1])==4:
      return [n[1][1][1]] + _a_bracket_num(n[1][3])
    else:
      raise ValueError
  def _a_comma_id(n):
    if len(n[1])==1:
      return []
    elif len(n[1])==3:
      return [n[1][1][1]] + _a_comma_id(n[1][2])
    else:
      raise ValueError
  dim = _a_bracket_num(n[1][1])
  ids = [n[1][2][1]] + _a_comma_id(n[1][3])
  result = []
  for id in ids:
    variables.add_variable_tag(id, dim)
    # allocate array if it is an array
    if dim:
      for d in dim:
        result.append("\tldc {}".format(d))
      if len(dim)==1:
        result.append("\tnewarray int")
      elif len(dim)>1:
        result.append("\tmultianewarray int{} {}-d".format(
          "[]"*len(dim), len(dim)))
      result.extend([
        "\tastore #{}".format(variables.get_variable_tag(id)[0])
      ])
  return result

def i_var(n, LR, code):
  def _a_bracket_arith_expr(n):
    if len(n[1])==1:
      return []
    else:
      return [c_arith_expr(n[1][1])] + _a_bracket_arith_expr(n[1][3])
  dim_code = _a_bracket_arith_expr(n[1][1])
  tag = variables.get_variable_tag(n[1][0][1])
  result = []
  if dim_code:
    result.append("\taload #{}".format(tag[0]))
    result.extend(dim_code[0])
    for dc in dim_code[1:]:
      result.append("\taaload")
      result.extend(dc)
  result.extend(code)
  if LR=="L":
    if dim_code:
      result.append("\tiastore")
    else:
      result.append("\tistore #{}".format(tag[0]))
  elif LR=="R":
    if dim_code:
      result.append("\tiaload")
    else:
      result.append("\tiload #{}".format(tag[0]))
  else:
    raise ValueError
  return result

def c_stmt(n):
  def _a_comma_var(n):
    if len(n[1])==1:
      return []
    else:
      return [n[1][1]] + _a_comma_var(n[1][2])
  def _a_comma_printable_expr(n):
    if len(n[1])==1:
      return []
    else:
      return [n[1][1]] + _a_comma_printable_expr(n[1][2])
  def _c_printable_expr(n):
    def __escape(s):
        return ''.join(['\\' + a if a=='\\' or a=='"' else a for a in s])
    if n[1][0][0]=='STR':
        return [
          '\tldc "{}"'.format(__escape(n[1][0][1])),
          "\tinvokestatic void print(String) @ Io"
        ]
    elif n[1][0][0]=='arith_expr':
      return c_arith_expr(n[1][0]) + [
        "\tinvokestatic void print(int) @ Io"
      ]
    else:
      raise ValueError
  def _c_scannable_expr(n):
    return i_var(n, "L", ["\tinvokestatic int scan() @ Io"])
  def _c_while(n):
    tag_start = tags.get_next_tag()
    tag_end = tags.get_next_tag()
    loop_tags.append((tag_start, tag_end))
    try:
      return ["{}:".format(tag_start)] + c_logic_expr(n[1][2]) + [
        "\tifeq {}".format(tag_end)
      ] + c_block_stmt(n[1][4]) + [
        "\tgoto {}".format(tag_start),
        "{}:".format(tag_end)
      ]
    finally:
      loop_tags.pop()
  def _c_scan(n):
    p_vars = [n[1][2]] + _a_comma_var(n[1][3])
    return sum(map(_c_scannable_expr, p_vars), [])
  def _c_print(n):
    p_exprs = [n[1][2]] + _a_comma_printable_expr(n[1][3])
    return sum(map(_c_printable_expr, p_exprs), [])
  def _c_println(n):
    return _c_print(n) + [
      "\tinvokestatic void newline() @ Io"
    ]
  if n[1][0][0]=='var':
    return i_var(n[1][0], "L", c_arith_expr(n[1][2]))
  elif n[1][0]=='if':
    tag1 = tags.get_next_tag()
    tag2 = tags.get_next_tag()
    result = c_logic_expr(n[1][2]) + [
      "\tifne {}".format(tag1)
    ]
    if len(n[1])==7:
      result.extend(c_block_stmt(n[1][6]))
    result.extend([
      "\tgoto {}".format(tag2),
      "{}:".format(tag1),
    ] + c_block_stmt(n[1][4]) + [
      "{}:".format(tag2)
    ])
    return result
  elif n[1][0]=='while':
    return _c_while(n)
  elif n[1][0]=='break':
    return ["\tgoto {}".format(loop_tags[-1][1])]
  elif n[1][0]=='continue':
    return ["\tgoto {}".format(loop_tags[-1][0])]
  elif n[1][0]=='scan':
    return _c_scan(n)
  elif n[1][0]=='print':
    return _c_print(n)
  elif n[1][0]=='println':
    return _c_println(n)
  elif n[1][0][0]=='block_stmt':
    return c_block_stmt(n[1][0])
  else:
    raise ValueError

def c_arith_expr(n):
  unary_op = {
    '+': [],
    '-': ["ineg"],
  }
  binary_op = {
    '+': ["iadd"],
    '-': ["isub"],
    '*': ["imul"],
    '/': ["idiv"],
  }
  if len(n[1])>1 and n[1][1] in binary_op:
    return c_arith_expr(n[1][0]) + c_arith_expr(n[1][2]) + binary_op[n[1][1]]
  elif n[1][0] in unary_op:
    return c_arith_expr(n[1][1]) + unary_op[n[1][0]]
  elif n[1][0][0]=='var': # rvalue
    return i_var(n[1][0], "R", [])
  elif isinstance(n[1][0], tuple) and n[1][0][0]=='NUM':
    return ["\tldc {}".format(n[1][0][1])]
  elif len(n[1])>1 and n[1][1][0]=='arith_expr':
    return c_arith_expr(n[1][1])
  else:
    raise ValueError

def c_logic_expr(n):
  # have not tested
  arith_cmp = {
    "<=": "\tif_icmple {}",
    "<" : "\tif_icmplt {}",
    ">=": "\tif_icmpge {}",
    ">" : "\tif_icmpgt {}",
    "==": "\tif_icmpeq {}",
    "!=": "\tif_icmpne {}",
  }
  logic_binary_cmp = {
    "||": ("ifne", 0, 1),
    "&&": ("ifeq", 1, 0),
  }
  if n[1][1] in logic_binary_cmp:
    tag1 = tags.get_next_tag()
    tag2 = tags.get_next_tag()
    (cmp_inst, long_val, short_val) = logic_binary_cmp[n[1][1]]
    return c_logic_expr(n[1][0]) + [
      "\t{} {}".format(cmp_inst, tag1)
    ] + c_logic_expr(n[1][2]) + [
      "\t{} {}".format(cmp_inst, tag1),
      "\tldc #{}".format(long_val),
      "\tgoto {}".format(tag2),
      "{}:".format(tag1),
      "\tldc #{}".format(short_val),
      "{}:".format(tag2)
    ]
  elif n[1][0]=='!':
    return c_logic_expr(n[1][1]) + [
      "\tldc #1",
      "\tixor"
    ]
  elif n[1][1] in arith_cmp:
    tag1 = tags.get_next_tag()
    tag2 = tags.get_next_tag()
    return c_arith_expr(n[1][0]) + c_arith_expr(n[1][2]) + [
      arith_cmp[n[1][1]].format(tag1),
      "\tldc #0",
      "\tgoto {}".format(tag2),
      "{}:".format(tag1),
      "\tldc #1",
      "{}:".format(tag2),
    ]
  elif n[1][1][0]=='logic_expr':
    return c_logic_expr(n[1][1])
  else:
    raise ValueError

def c_empty(n):
  """ Empty code.
  """
  return []

