from ply import lex, yacc
""" The lexer and parser of `cmm' language, powered by Python
    Lex-Yacc(PLY).
    Official site of PLY: http://www.dabeaz.com/ply
"""

""" The cmm lexer constructed from the rules defined in module
    lexer_rule
"""
from lexer_rule import *
lexer = lex.lex()

""" The cmm parser constructed from the rules defined in module
    lexer_rule and parser_rule
"""
from parser_rule import *
parser = yacc.yacc()

