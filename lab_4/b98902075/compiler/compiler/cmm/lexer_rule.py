import re
import string

from ply import lex

""" Lexer rule of `cmm' language.  Used to construct the cmm lexer.
    Powered by PLY.  For its naming convention and usage plase go to
    its official website, mentioned in compiler.cmm.__init__.
"""

keywords = (
  'int',
  'if',
  'else',
  'while',
  'break',
  'continue',
  'scan',
  'print',
  'println',
)

keywords_dict = dict(map(lambda k: (k, k.upper()), keywords))

""" Single character operators.
"""
literals = '()[]{},;='

tokens = (
  'LEQ', 'LT', 'GEQ', 'GT', 'EQ', 'NEQ',
  'LOGIC_OR', 'LOGIC_AND', 'LOGIC_NOT',
  'PLUS', 'MINUS', 'TIMES', 'DIVIDE',
  'NUM',
  'STR',
  'ID',
) + tuple(keywords_dict.values())

""" Compare
"""
t_LEQ = re.escape(r'<=')
t_LT  = re.escape(r'<')
t_GEQ = re.escape(r'>=')
t_GT  = re.escape(r'>')
t_EQ  = re.escape(r'==')
t_NEQ = re.escape(r'!=')

""" Logical
"""
t_LOGIC_OR  = re.escape(r'||')
t_LOGIC_AND = re.escape(r'&&')
t_LOGIC_NOT = re.escape(r'!')

""" Arithmetic
"""
t_PLUS   = re.escape(r'+')
t_MINUS  = re.escape(r'-')
t_TIMES  = re.escape(r'*')
t_DIVIDE = re.escape(r'/')

""" Number
"""
@lex.TOKEN(r'\d+')
def t_NUM(t):
  t.value = (t.type, int(t.value))
  return t

""" String
"""
@lex.TOKEN(r'"([{}]|\\\\|\\\")*?"'.format(re.escape(str(string.printable).replace('\\',''))))
def t_STR(t):
  def remove_slash(s):
    return s[1:] if s==r'\\' or s==r'\"' else s
  def escape(s):
    return ''.join(map(remove_slash, re.findall(r'\\\\|\\\"|.', s[1: -1])))
  t.value = (t.type, escape(t.value))
  return t

""" Identifier and keywords.
"""
@lex.TOKEN(r'[a-zA-Z]\w*')
def t_ID(t):
  if t.value in keywords_dict:
    t.type = keywords_dict[t.value]
  else:
    t.value = ('ID', t.value)
  return t

def t_error(t):
  """ LexError without any recovery.
  """
  line = t.lexer.lineno
  column = t.lexer.lexpos - t.lexer.lexdata.rfind('\n', 0, t.lexer.lexpos)
  raise lex.LexError("Lexical error at line {}, column {}.".format(
      line, column
    ),
    t.lexer.lexdata[t.lexer.lexpos:]
  )

@lex.TOKEN(r'\/\*(.|\n)*?\*\/|\s+')
def t_ignore_COMMENT(t):
  t.lexer.lineno += t.value.count('\n')

