import argparse
import sys

import ply.lex
import compiler.cmm as cmm

""" The main module of lab_3.  The additional feature, tree plotting, is
    also implemented here.
    For its usage please see the argparser object.
"""

def write_tree(file, root):
  """ Given the root of the tree, print the tree info file.
  """
  def _write_node(value, prefix, haschild, islast):
    """ Write a node.
    """
    pre = ''.join(map(lambda p: "| " if p else "  ", prefix))
    pre += "\\" if islast else "|"
    pre += "-"
    pre += "+" if haschild else "-"
    pre += "- "
    file.write(pre + value + "\n")
  def _write_tree(treenode, lv, prefix, hold, islast):
    """ Worker.
    """
    if isinstance(treenode, tuple):
      if isinstance(treenode[1], tuple):
        _write_node(treenode[0], prefix, haschild=treenode[1], islast=islast)
        prefix.append(hold)
        hold = True
        for child in treenode[1]:
          if child is treenode[1][-1]:
            hold = False
          _write_tree(child, lv+1, prefix, hold, child is treenode[1][-1])
        hold = prefix.pop()
      else:
        _write_node('Token: {}, Attr: "{}"'.format(treenode[0],str(treenode[1])), prefix, haschild=False, islast=islast)
    else:
      _write_node("Token: {}".format(treenode.upper()), prefix, haschild=False, islast=islast)
  _write_tree(root, 0, [], False, True)

argparser = argparse.ArgumentParser(description=""" Check the cmm
  program if there is any syntax error.  It can also show the parsing
  tree if the syntax of the cmm program is correct.  It reads from stdin
  and writes to stdout by default. """
)
argparser.add_argument('-i', '--input', metavar="file_name", help="""
  specify input cmm program file by name instead of stdin. """
)
argparser.add_argument('-o', '--output', metavar="file_name", help="""
  specify output file by name if you instead of stdout. """
)
argparser.add_argument('-t', '--tree', action="store_true", help="""
  show parsing tree instead of Pass/Fail. """
)
 
def __main__():
  args = argparser.parse_args()
  with open(args.input, 'r') if args.input else sys.stdin as input_file:
    content = ''.join(input_file)
  try:
    result = cmm.parser.parse(content)
    with open(args.output, 'w') if args.output else sys.stdout as output_file:
      if args.tree:
        if result:
          write_tree(output_file, result)
      else:
        fp = "Pass\n" if result is not None else "Fail\n"
        output_file.write(fp)
  except (ply.lex.LexError, SyntaxError) as e:
    sys.stderr.write(str(e)+"\n")

if __name__=='__main__':
  __main__()

